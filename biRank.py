## This version of biRank implements an interaction modifying the probability of chosing 
## the next node. In this version I try to implement another approach to the computting of probabilities. 
import math
from numpy import random as rd
import IO
import copy
import numpy as np
from collections import Counter as ct
from itertools import accumulate

class surfer :
  """This class represents the object "surfer" which is charatherized solely by 
  his neighbors (successors) list (and maybe the others)"""
  
  # constructor 
  def __init__(self,node,neighborsList=[]) :
    self.nl = neighborsList
    self.n = node

def nextNode(neighbors,pops,gamma = 1) :
  """This function choses randomly the next node, weighting the probabilities according to 
  the populations of the surfers and a parameter gamma."""
  pro =  [pops[n]**gamma for n in neighbors]
  Z = sum(pro)
  proZ = [h/Z for h in pro]
  rand = rd.random_sample()
  bins = list(accumulate(proZ))
  bins.insert(0,0.0)
  index = np.digitize(rand,bins)
  return neighbors[index-1]

def biRank(graph,iterations,d = 0.85,SDensity = 1,percentTol = 1,gamma1 = 1,gamma2 = 1) :
  """This function takes a bipartite graph as input and returns the pair of agentRanks 
  with the rank of the nodes of the two projections, computed via a random surfer approach
  in which there are two kinds of surfers, one for each graph projection. When a surfer of one
  kind encounters one or more surfer of the other kind on one of the - possible - target nodes,
  the probability of going to that node (uniform by default) is modified according to n^gamma, where n is the surfer
  population of that node. So a positive gamma means an attractive interaction, while a negative gamma means
  a repulsive intraction."""

  # instantiate some useful quantities
  projected1,projected2 = IO.projections(graph)
  projected1list=list(projected1)
  projected2list=list(projected2)
  N1 = len(projected1)
  N2 = len(projected2)
  N = max(N1,N2)
  surfNumber1 = max(math.ceil(N1*SDensity),1)
  surfNumber2 = max(math.ceil(N2*SDensity),1)
  print('sn: ',surfNumber1,surfNumber2)
  prob = np.fromiter([True for i in range(int(d*100))]+[False for j in range(int((1-d)*100))],bool,100)
  pageRank1 = dict.fromkeys(projected1,1)
  pageRank2 = dict.fromkeys(projected2,1)
  it = 0 # this will serve as counter for the pagerank
  populations = dict.fromkeys(graph.nodes,1)
  first = True
  s = SDensity
  if SDensity == 0. :
    s = 1/N
  print('s: ', math.ceil(s))
  
  # compute, once, the dict  with all the neighbors of all nodes
  neighD = {}
  for f in graph.nodes :
    neighD[f]=np.asarray(list(graph.neighbors(f)))
  
  # compute the initial positions for the surfers of type 1
  nodes1 = rd.choice(projected1list,size = surfNumber1)
  # instantiate the list containing the surfers,
  sList1 =[surfer(x1,neighD[x1]) for x1 in nodes1]
  # compute the initial positions for the surfers of type 2
  nodes2 = rd.choice(projected2list,size = surfNumber2)
  # instantiate the list containing the surfers,
  sList2 =[surfer(x2,neighD[x2]) for x2 in nodes2]

  # refresh populations
  for n1 in nodes1 :
    populations[n1]+=1
  for n2 in nodes2 :
    populations[n2]+=1

  #main loop
  for m in range(iterations) :
  
    if it == 0 :
      previous1 = copy.deepcopy(pageRank1)
      previous2 = copy.deepcopy(pageRank2)
    it += 1
    

    for s1 in sList1 :
      populations[s1.n]-=1
      # if the surfer choses to do a hyperleap
      if not rd.choice(prob) or s1.nl.size == 0 :
        s1.n = rd.choice(list(projected1-{s1.n}))   
      # if the surfer choses to follow the links  
      else :
        #middle step
        stand1 = nextNode(s1.nl,populations,gamma1)
        #step
        s1.nl = np.asarray(list(set(neighD[stand1])-{s1.n}))
        if s1.nl.size == 0 :
          s1.n = rd.choice(list(projected1-{s1.n})) #un'altro ipersalto  
        else :
          s1.n = rd.choice(s1.nl)
      pageRank1[s1.n]+=1
      s1.nl = neighD[s1.n]
      populations[s1.n]+=1

    # loop over surfers of tipe 2
    for s2 in sList2 :
      populations[s2.n]-=1
      # if the surfer choses to do a hyperleap
      if not rd.choice(prob) or s2.nl.size == 0 :
        s2.n = rd.choice(list(projected2-{s2.n}))   
      # if the surfer choses to follow the links  
      else :
        #middle-step:
        stand2 = nextNode(s2.nl,populations,gamma2)
        #step
        s2.nl = np.asarray(list(set(neighD[stand2])-{s2.n}))  
        if s2.nl.size == 0 :
          s2.n = rd.choice(list(projected2-{s2.n})) #un'altro ipersalto  
        else :
          s2.n = rd.choice(s2.nl)
      pageRank2[s2.n]+=1
      s2.nl = neighD[s2.n]
      populations[s2.n]+=1

    # check for convergence 
    if it == math.ceil(1/s) :
      if first :
        first = False
        it = 0
      else :
        it = 0
       # print("current step",m)
        e1 = max([abs(previous1[x]-pageRank1[x])/previous1[x] for x in pageRank1])
        e2 = max([abs(previous2[y]-pageRank2[y])/previous2[y] for y in pageRank2])
       # print("error: ",e*100)
        if e1*100 < percentTol and e2*100 < percentTol :
          print("convergence reached, number of iterations: ",m+1)
          print("Final error 1: ", e1*100, " %","\nFinal error 2: ", e2*100, " %")
          break 
      
  print("iteration check ",m+1)  
  for z in pageRank1 :
    pageRank1[z] = pageRank1[z]/((m+1)*surfNumber1)

  for w in pageRank2 :
    pageRank2[w] = pageRank2[w]/((m+1)*surfNumber2)
  
  return pageRank1,pageRank2


def degreeRank(graph) :

  projected1,projected2 = IO.projections(graph)
  degree1 = [graph.degree(n) for n in projected1]
  degree2 = [graph.degree(m) for m in projected2]

  rank1 = dict(zip(projected1,degree1))
  rank2 = dict(zip(projected2,degree2))

  return rank1,rank2
