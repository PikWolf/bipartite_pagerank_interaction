import networkx as nx
from numpy import random as rd
from tqdm import tqdm 
import operator
import IO



def singleAgentRank(graph,position,iterations,d) :
  """This function takes a DIRECTED graph as an input and returns the pagerank of the"
  nodes, using the random surfer method. """
   
  if graph.is_directed() == False :
    print('it must be directed')
    return False
  
  N = nx.number_of_nodes(graph)
  prob = [True for i in range(int(d*100))]+[False for j in range(int((1-d)*100))]
  pageRank = dict.fromkeys(list(graph.nodes),1.0/N)
 
  # main iteration loop
  node = position
  for n in tqdm(range(iterations)) :
    neighbors = list(graph.neighbors(node))
    others = list(set(graph.nodes)-set(neighbors))

    if not rd.choice(prob) or not neighbors :
      node = rd.choice(others)
    else :
      node = rd.choice(neighbors)
    pageRank[node]+=1

  for m in pageRank : #non lo posso includere già nel loop di prima?
  #  print("prima ",pageRank[m])
    pageRank[m]= pageRank[m]/iterations 
  #  print("dopo ",pageRank[m]) 
  
  return pageRank

def AgentRank(graph,iterations,d,loops = None) :
  """This function executes the singlePageRank starting from different initial positions
  and then averages the result. if provided, loops is the number different initial positions
  to start with. The default is one iteration per node of the graph. Otherwise loops nodes are 
  selected at random. """

  # work out the positions to use as initial ones
  if loops == None :
    positions = list(graph.nodes)
  elif loops < 1 :
    raise ValueError('you have inserted a number < 1')
  else :
    positions = rd.choice(list(graph.nodes),size=loops)
  
  # instantiate the needed data structures
  AgentRank = {}
  AgentRankArray = []
  
  # initialize the AgentRank dictionary
  for k in graph.nodes :
    AgentRank[k] = 0
    
  # main loop: for each initial position executes a singleAgentRank
  for i in tqdm(positions) :
    AgentRankArray.append(singleAgentRank(graph,i,iterations,d))

  # do the needed averages over the number of initial conditions used
  for l in range(len(positions)) :
    for m in AgentRankArray[l] :
#     print("m= ", m, "l= ",l, "------",AgentRankArray[l][m], "----")
      AgentRank[m] += AgentRankArray[l][m]/len(positions)

  return AgentRank


###########################################################################
##################### Alcune funzioni di controllo e verifica #############


def convergence(graph,d,e) :
  N = nx.number_of_nodes(graph)
  iterations = 0
  appo = dict.fromkeys(list(graph.nodes),1.0/N)
  error = e*N+1
  
  while error>e*N : 
    iterations += 100
    print("iterations: ",iterations)
    rank = AgentRank(graph,iterations,d)
    error = sum([abs(appo[x]-rank[x])/appo[x] for x in rank])
    print("error: ",error,"  appo: ",appo, " rank:  ",rank)
    appo = rank
  return

def comparison(graph,d,e) : 
   N = nx.number_of_nodes(graph)
   iterations = 0
   error = e*N+1
   prank = nx.pagerank(graph)
   
   while error>e*N :
     iterations += 500
     print("iterations: ",iterations)
     rank = AgentRank(graph,iterations,d)
     error = sum([abs(rank[x]-prank[x])/prank[x] for x in prank])
     print("error: ",error,"  pagerank ",prank, " rank:  ",rank)
   return

def orderComparison(graph,d,maxIterations) :

  pg = IO.rank(nx.pagerank(graph),nodes=True)
  for iterations in range(10,maxIterations,500) :
    ar = IO.rank(AgentRank(graph,iterations,d),nodes=True)

    print("iterations: ",iterations)
    print("pg: ",pg,"  ar",ar,"  ",pg == ar)
  
#def transientCheck(graph,maxIterations) :
  
