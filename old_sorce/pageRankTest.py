import networkx as nx
from numpy import random as rd
from tqdm import tqdm 
import operator



def singleAgentRank(graph,position,iterations,d) :
  """This function takes a DIRECTED graph as an input and returns the pagerank of the"
  nodes, using the random surfer method. """
   
  if graph.is_directed() == False :
    print('heeeey')
    return False
   
  else :
    prob = [True for i in range(int(d*100))]+[False for j in range(int((1-d)*100))]
    pageRank={}

    for i in graph.nodes :
      pageRank[i]=0

    # main iteration loop
    for n in tqdm(range(iterations)) :
      neighbors = list(graph.successors(position))
      others = list(set(graph.nodes)-set(neighbors))

      if not rd.choice(prob) or not neighbors :
        node = rd.choice(others)
      else :
        node = rd.choice(neighbors)
      pageRank[node]+=1

    for m in pageRank :
    #  print("prima ",pageRank[m])
      pageRank[m]= pageRank[m]/iterations 
    #  print("dopo ",pageRank[m]) 
    return pageRank

def AgentRank(graph,iterations,d,loops = None) :
  """This function executes the singlePageRank starting from different initial positions
  and then averages the result. if provided, loops is the number different initial positions
  to start with. The default is one iteration per node of the graph. Otherwise loops nodes are 
  selected at random. """

  # work out the positions to use as initial ones
  if loops == None :
    positions = list(graph.nodes)
  elif loops < 1 :
    raise ValueError('you have inserted a number < 1')
  else :
    positions = rd.choice(list(graph.nodes),size=loops)
  
  # instantiate the needed data structures
  AgentRank = {}
  AgentRankArray = []
  
  # initialize the AgentRank dictionary
  for k in graph.nodes :
    AgentRank[k] = 0
    
  # main loop: for each initial position executes a singleAgentRank
  for i in tqdm(positions) :
    AgentRankArray.append(singleAgentRank(graph,i,iterations,d))

  # do the needed averages over the number of initial conditions used
  for l in range(len(positions)) :
    for m in AgentRankArray[l] :
#     print("m= ", m, "l= ",l, "------",AgentRankArray[l][m], "----")
      AgentRank[m] += AgentRankArray[l][m]/len(positions)

  return AgentRank


g = nx.complete_graph(50)

#g = nx.read_adjlist("email-EU-core.txt")
d = g.to_directed()
pg = nx.pagerank(d)
print("pageRank",pg.items())
ar = AgentRank(d,25000,0.85)
print("\n\n ___________________________________________________ \n\n")
print("agentRank: ","\n iterations: 25000  d: 0.85  loops: 50 \n",ar.items())


sorted_pg = sorted(pg.items(),key=operator.itemgetter(1))
sorted_ar = sorted(ar.items(),key=operator.itemgetter(1))

print(sorted_pg, "\n\n\n", sorted_ar)
