## Thi version of biRank implements an interaction modifying the probability of chosing 
## the next node. 
import math
import networkx as nx
from numpy import random as rd
import operator
import IO
import copy
import numpy as np
from collections import Counter as ct

class surfer :
  """This class represents the object "surfer" which is charatherized solely by 
  his neighbors (successors) list (and maybe the others)"""
  
  # constructor 
  def __init__(self,node,neighborsList=[]) :
    self.nl = neighborsList
    self.n = node

def populations(sList) :
  """This function returns the array with the pairs (node,population) computed from
  the list of surfers. """
  nod = [i.n for i in sList]
  c = ct(nod)
  return c

def probabilities(neighbors,pops,gamma = 1) :
  """This function computes the probabilities for the next step from the given node and returns the
  right weighted array."""
  pro = [pops[ki]**gamma for ki in neighbors]
  probs = [neighbors[i] for i in range(len(neighbors)) for j in range(pro[i]) ] + list(neighbors)
  return probs

def AgentRank(graph,iterations,d,surfNumber = 1,percentTol = 1,gamma = 1) :
  """This function takes a DIRECTED graph as an input and returns the pagerank of the"
  nodes, using the random surfer method. """

  # instantiate some useful quantities
  projected1,projected2 = IO.projections(graph)
  projected1list=list(projected1)
  projected2list=list(projected2)
  N = max(len(projected1),len(projected2))
  prob = np.fromiter([True for i in range(int(d*100))]+[False for j in range(int((1-d)*100))],bool,100)
  pageRank1 = dict.fromkeys(projected1,(1/len(projected1))*0.1)
  pageRank2 = dict.fromkeys(projected2,(1/len(projected2))*0.1)
  it = 0 # this will serve as counter for the pagerank
  
  # compute, once, the dict  with all the neighbors of all nodes
  neighD = {}
  for f in graph.nodes :
    neighD[f]=list(graph.neighbors(f))
  
  # compute the initial positions for the surfers of type 1
  nodes1 = rd.choice(list(projected1),size = surfNumber)
  # instantiate the list containing the surfers,
  sList1 =[surfer(x1,neighD[x1]) for x1 in nodes1]
  # compute the initial positions for the surfers of type 2
  nodes2 = rd.choice(list(projected2),size = surfNumber)
  # instantiate the list containing the surfers,
  sList2 =[surfer(x2,neighD[x2]) for x2 in nodes2]
  #main loop
  for m in range(iterations) :
    if it == 0 :
     # print("previous step: ",m)
      previous1 = copy.deepcopy(pageRank1)
      previous2 = copy.deepcopy(pageRank2)
    it += 1
    #print("it ",it)  
    

    for s1 in sList1 :
      # if the surfer choses to do a hyperleap
      if not rd.choice(prob) or len(s1.nl) == 0 :
        s1.n = rd.choice(projected1list)
        pageRank1[s1.n]+=1
        s1.nl = neighD[s1.n]
      # if the surfer choses to follow the links  
      else :
        # compute the probabilities
        pro = probabilities(s1.nl,populations(sList2),gamma)
        #middle-step:
        stand1 = rd.choice(pro)
        #step
        s1.nl = neighD[stand1]  # ocio, qui dovrei togliere il precedente??
        s1.n = rd.choice(s1.nl)
        pageRank1[s1.n]+=1
        s1.nl = neighD[s1.n]

    # loop over surfers of tipe 2
    for s2 in sList2 :
      # if the surfer choses to do a hyperleap
      if not rd.choice(prob) or len(s2.nl) == 0 :
        s2.n = rd.choice(projected2list)
        pageRank2[s2.n]+=1
        s2.nl = neighD[s2.n]
      # if the surfer choses to follow the links  
      else :
        #compute the probabilities
        pro = probabilities(s2.nl,populations(sList1),gamma)
        #middle-step:
        stand2 = rd.choice(pro)
        #step
        s2.nl = neighD[stand2]
        s2.n = rd.choice(s2.nl)
        pageRank2[s2.n]+=1
        s2.nl = neighD[s2.n]

    # check for convergence every 10*N iterations, where N is the number of nodes of the graph.
    if it == math.ceil(N/surfNumber) :
      it = 0  
     # print("current step",m)
      e1 = max([abs(previous1[x]-pageRank1[x])/previous1[x] for x in pageRank1])
      e2 = max([abs(previous2[y]-pageRank2[y])/previous2[y] for y in pageRank2])
     # print("error: ",e*100)
      if e1*100 < percentTol and e2*100 < percentTol :
        print("convergence reached, number of iterations: ",m+1)
        print("Final error 1: ", e1*100, " %","\nFinal error 2: ", e2*100, " %")
        break 
      
  print("iteration check ",m+1)  
  for z in pageRank1 :
    pageRank1[z] = pageRank1[z]/((m+1)*surfNumber)

  for w in pageRank2 :
    pageRank2[w] = pageRank2[w]/((m+1)*surfNumber)
  
  return pageRank1,pageRank2

