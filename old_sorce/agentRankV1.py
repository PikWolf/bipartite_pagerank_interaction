import networkx as nx
from numpy import random as rd
from tqdm import tqdm 
import operator
import IO
import copy

class surfer :
   """This class represent the object "surfer" which is charatherized solely by 
   his neighbors (successors) list (and maybe the others)"""
   
   # constructor 
   def __init__(self,node,neighborsList=[]) :
     self.nl = neighborsList
     self.n = node


def AgentRank(graph,iterations,d,surfNumber = 1,percentTol = 1) :
  """This function takes a DIRECTED graph as an input and returns the pagerank of the"
  nodes, using the random surfer method. """
   
  if graph.is_directed() == False :
    print('it must be directed')
    return False
   
  else :

    # instantiate some useful quantities
    N = nx.number_of_nodes(graph)
    prob = [True for i in range(int(d*100))]+[False for j in range(int((1-d)*100))]
    NL = list(graph.nodes)
    pageRank = dict.fromkeys(NL,0.001)
    previous = dict.fromkeys(NL,1)
    it = 0 # this will serve as counter for the pagerank
    
    # compute the initial positions for the surfers
    nodes = list(rd.choice(NL,size = surfNumber))
    # instantiate the list containing the surfers,
    sList = [surfer(x,list(graph.neighbors(x))) for x in nodes]
    
    #main loop
    for m in tqdm(range(iterations)) :
      # loop over surfers
      for s in sList :
        # if the surfer choses to do a hyperleap
        if not rd.choice(prob) or not s.nl :
          s.n = rd.choice(NL)
        # if the surfer choses to follow the links  
        else :
          s.n = rd.choice(s.nl)
        # add 1 to the pagerank of the current node and
        # compute the new neighbors list
        pageRank[s.n]+=1
        s.nl = list(graph.neighbors(s.n))
    #  print(pageRank)
      # check for convergence every 10*N iterations, where N is the number of nodes of the graph.
      if (m > 0) and (m % (10*N)) == 0 :
    #    print(m)
    #    print(pageRank," ",previous)
        e =  sum([abs(previous[x]-pageRank[x])/previous[x] for x in pageRank])
        if e*100 < percentTol*N :
          print("error:   ",e*100/N)
          it = m
          print("it     ",it) 
          break
      if (m % 10) == 0 :
       # print(pageRank, " OK ",previous)
        previous = copy.deepcopy(pageRank)
      if (m == (iterations - 1)) :
        print("no convergence")
        it = m
    
    for z in pageRank :
      pageRank[z] = pageRank[z]/(it*surfNumber)

    return pageRank



###########################################################################
##################### Alcune funzioni di controllo e verifica #############

# questa non serve più
def convergence(graph,d,e,surfNumber = 1) :
  N = nx.number_of_nodes(graph)
  iterations = 0
  appo = dict.fromkeys(list(graph.nodes),1.0/N)
  error = e*N+1
  
  while error>e*N : 
    iterations += 20
    print("iterations: ",iterations)
    rank = AgentRank(graph,iterations,d,surfNumber)
    error = sum([abs(appo[x]-rank[x])/appo[x] for x in rank])
    print("error: ",error/N*100,"%","  appo: ",appo, " rank:  ",rank)
    appo = rank
  return
# ancge questa per il momento
def comparison(graph,d,e,surfNumber = 1) : 
   N = nx.number_of_nodes(graph)
   iterations = 0
   error = e*N+1
   prank = nx.pagerank(graph)
   
   while error>e*N :
     iterations +=20
     print("iterations: ",iterations)
     rank = AgentRank(graph,iterations,d,surfNumber)
     error = sum([abs(rank[x]-prank[x])/prank[x] for x in prank])
     print("error: ",error/N*100,"%","  pagerank ",prank, " rank:  ",rank)
   return

def orderComparison(graph,d,maxIterations,surfNumber = 1,tol = 1) :

  pg = IO.rank(nx.pagerank(graph),nodes=True)
  ar = IO.rank(AgentRank(graph,maxIterations,d,surfNumber,percentTol = tol),nodes=True)

  print("pg: ",pg,"  ar",ar,"  ",pg == ar)
  
#def transientCheck(graph,maxIterations) :
  
