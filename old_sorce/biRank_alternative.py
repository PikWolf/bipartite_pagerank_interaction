## agentRank for bipartite undirected graphs, with interaction. Here the check
## is made only on the random elected next-node. Furthermore, the going on procedure is
## made through a weight mechanism
import math
import networkx as nx
from numpy import random as rd
from tqdm import tqdm 
import operator
import IO
import copy
import numpy as np

class surfer :
  """This class represents the object "surfer" which is charatherized solely by 
  his neighbors (successors) list (and maybe the others)"""
  
  # constructor 
  def __init__(self,node,neighborsList=[]) :
    self.nl = neighborsList
    self.n = node

def biNeighbors(bigraph,weighP1,weighP2,node) : 
  """projection must be 0 or 1, that are the number used by networkx as attributes to
  distinguish the two projections of the bigraph. Returns the list of neighbors of node
  in its prjection"""
  
  nList = []
  nListd = [] 
  if bigraph.nodes[node]['bipartite'] == 0 :
    projection = weighP1
  else :
    projection = weighP2

  nList = list(nx.neighbors(projection,node))
  for n in nList :
    for i in range(projection.edges[(node,n)]['weight']) :
      nListd.append(n)
  
  return nListd
  

def AgentRank(graph,iterations,d,surfNumber = 1,percentTol = 1) :
  """This function takes a DIRECTED graph as an input and returns the pagerank of the"
  nodes, using the random surfer method. """
   

  # instantiate some useful quantities
  projected1,projected2 = IO.weightedProjections(graph)
  print('yes')
  projected1list=list(projected1)
  projected2list=list(projected2)
  N = max(len(projected1),len(projected2))
  prob = np.fromiter([True for i in range(int(d*100))]+[False for j in range(int((1-d)*100))],bool,100)
  NL = np.asarray(graph.nodes)
  pageRank1 = dict.fromkeys(projected1,(1/len(projected1))*0.1)
  pageRank2 = dict.fromkeys(projected2,(1/len(projected2))*0.1)
  it = 0 # this will serve as counter for the pagerank
  try1 = 0
  try2 = 0
  appo = []
  # compute, once, the dict  with all the neighbors of all nodes
  neighD = {}
  for f in tqdm(graph.nodes) :
    neighD[f]=np.asarray(biNeighbors(graph,projected1,projected2,f))
  
  # compute the initial positions for the surfers of type 1
  nodes1 = rd.choice(list(projected1),size = surfNumber)
  occupied1 = np.array(nodes1)
  # instantiate the list containing the surfers,
  sList1 =[surfer(x1,neighD[x1]) for x1 in nodes1]
    
  # compute the initial positions for the surfers of type 2
  nodes2 = rd.choice(list(projected2),size = surfNumber)
  occupied2 = np.array(nodes2)
  # instantiate the list containing the surfers,
  sList2 =[surfer(x2,neighD[x2]) for x2 in nodes2]
  
  #main loop
  for m in tqdm(range(iterations)) :
    if it == 0 :
     # print("previous step: ",m)
      previous1 = copy.deepcopy(pageRank1)
      previous2 = copy.deepcopy(pageRank2)
    it += 1
    #print("it ",it)  
    # loop over surfers of tipe 1
    for s1 in sList1 :
      indexes = []
      for oc in occupied1 :
        indexes +=list(np.where(s1.nl == oc)[0])
      appoNl = np.delete(s1.nl,indexes)
      # if the surfer choses to do a hyperleap
      if not rd.choice(prob) or appoNl.size == 0 :
        s1.n  = rd.choice(projected1list)
      # if the surfer choses to follow the links  
      else :
        s1.n = rd.choice(appoNl)
      # now check the presence of surfers of type 2
      # add 1 to the pagerank of the current node and
      # compute the new neighbors list
      pageRank1[s1.n]+=1
      s1.nl = neighD[s1.n]
      
      # else do nothing

    for s2 in sList2 :
      indexes = []
      for oc in occupied2 :
        indexes +=list(np.where(s2.nl == oc)[0])
      appoNl = np.delete(s2.nl,indexes)
      # if the surfer choses to do a hyperleap
      if not rd.choice(prob) or appoNl.size == 0 :
        s2.n  = rd.choice(projected2list)
      # if the surfer choses to follow the links  
      else :
        s2.n = rd.choice(appoNl)
      # now check the presence of surfers of type 2
      # add 1 to the pagerank of the current node and
      # compute the new neighbors list
      pageRank2[s2.n]+=1
      s2.nl = neighD[s2.n]
      # else do nothing

    # check for convergence every 10*N iterations, where N is the number of nodes of the graph.
    if it == math.ceil(N/surfNumber) :
      it = 0  
     # print("current step",m)
      e1 = max([abs(previous1[x]-pageRank1[x])/previous1[x] for x in pageRank1])
      e2 = max([abs(previous2[y]-pageRank2[y])/previous2[y] for y in pageRank2])
     # print("error: ",e*100)
      if e1*100 < percentTol and e2*100 < percentTol :
        print("convergence reached, number of iterations: ",m+1)
        print("Final error 1: ", e1*100, " %","\nFinal error 2: ", e2*100, " %")
        break 
      
  print("iteration check ",m+1)  
  for z in pageRank1 :
    pageRank1[z] = pageRank1[z]/((m+1)*surfNumber)

  for w in pageRank2 :
    pageRank2[w] = pageRank2[w]/((m+1)*surfNumber)
  
  return pageRank1,pageRank2

