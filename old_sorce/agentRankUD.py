## agentrank for undirected graphs ##
import math
import networkx as nx
from numpy import random as rd
from tqdm import tqdm 
import operator
import IO
import copy

class surfer :
   """This class represents the object "surfer" which is charatherized solely by 
   his neighbors (successors) list (and maybe the others)"""
   
   # constructor 
   def __init__(self,node,neighborsList=[]) :
     self.nl = neighborsList
     self.n = node


def AgentRank(graph,iterations,d,surfNumber = 1,percentTol = 1) :
  """This function takes a DIRECTED graph as an input and returns the pagerank of the"
  nodes, using the random surfer method. """
   

  # instantiate some useful quantities
  N = nx.number_of_nodes(graph)
  prob = [True for i in range(int(d*100))]+[False for j in range(int((1-d)*100))]
  NL = list(graph.nodes)
  pageRank = dict.fromkeys(NL,(1/N)*0.1)
  it = 0 # this will serve as counter for the pagerank
  
  # compute the initial positions for the surfers
  nodes = list(rd.choice(NL,size = surfNumber))
  # instantiate the list containing the surfers,
  sList = [surfer(x,list(graph.neighbors(x))) for x in nodes]
  
  #main loop
  for m in tqdm(range(iterations)) :
    if it == 0 :
      print("previous step: ",m)
      previous = copy.deepcopy(pageRank)
    it += 1
    print("it ",it)  
    # loop over surfers
    for s in sList :
      # if the surfer choses to do a hyperleap
      if not rd.choice(prob) or not s.nl :
        s.n = rd.choice(NL)
      # if the surfer choses to follow the links  
      else :
        s.n = rd.choice(s.nl)
      # add 1 to the pagerank of the current node and
      # compute the new neighbors list
      pageRank[s.n]+=1
      s.nl = list(graph.neighbors(s.n))

    # check for convergence every 10*N iterations, where N is the number of nodes of the graph.
    if it == math.ceil(N/surfNumber) :
      it = 0  
      print("current step",m)
      e = max([abs(previous[x]-pageRank[x])/previous[x] for x in pageRank])
      print("error: ",e*100)
      if e*100 < percentTol :
        print("convergence reached, number of iterations: ",m)
        break 
      
  print("iteration check ",m+1)  
  for z in pageRank :
    pageRank[z] = pageRank[z]/((m+1)*surfNumber)

  return pageRank



###########################################################################
##################### Alcune funzioni di controllo e verifica #############

# questa non serve più
def convergence(graph,d,e,surfNumber = 1) :
  N = nx.number_of_nodes(graph)
  iterations = 0
  appo = dict.fromkeys(list(graph.nodes),1.0/N)
  error = e*N+1
  
  while error>e*N : 
    iterations += 20
    print("iterations: ",iterations)
    rank = AgentRank(graph,iterations,d,surfNumber)
    error = sum([abs(appo[x]-rank[x])/appo[x] for x in rank])
    print("error: ",error/N*100,"%","  appo: ",appo, " rank:  ",rank)
    appo = rank
  return
# ancge questa per il momento
def comparison(graph,d,e,surfNumber = 1,tol = 1) : 
   N = nx.number_of_nodes(graph)
   iterations = 0
   error = e*N+1
   prank = nx.pagerank(graph)
   
   while error>e*N :
     iterations +=20
     print("iterations: ",iterations)
     rank = AgentRank(graph,iterations,d,surfNumber,tol)
     error = sum([abs(rank[x]-prank[x])/prank[x] for x in prank])
     print("error: ",error/N*100,"%","  pagerank ",prank, " rank:  ",rank)
   return

def orderComparison(graph,d,maxIterations,surfNumber = 1,tol = 1) :

  pg = IO.rank(nx.pagerank(graph),nodes=True)
  ar = IO.rank(AgentRank(graph,maxIterations,d,surfNumber,percentTol = tol),nodes=True)

  print("pg: ",pg,"  ar",ar,"  ",pg == ar)
  
#def transientCheck(graph,maxIterations) :
def aR_K_Comparison(graph,d,maxIterations,surfNumber=1,tol=1) :
  ar = IO.rank(AgentRank(graph,maxIterations,surfNumber,percentTol = tol),nodes=True) 
  k =  IO.rank(dict(nx.degree(graph)),nodes=True)
  
  print("ar: ", ar," degree: ",k,ar == k)
