## Thi version of biRank implements an interaction modifying the probability of chosing 
## the next node. 
import math
from numpy import random as rd
import IO
import copy
import numpy as np
from collections import Counter as ct

class surfer :
  """This class represents the object "surfer" which is charatherized solely by 
  his neighbors (successors) list (and maybe the others)"""
  
  # constructor 
  def __init__(self,node,neighborsList=[]) :
    self.nl = neighborsList
    self.n = node

def probabilities(neighbors,pops,sListOth,gamma = 1) :
  relevant = list(set(neighbors) &  set([s.n for s in sListOth]))
  #print("relevnt ---  :",relevant)
  pro =  [int(pops[n]**gamma) for n in relevant]
  return neighbors + [relevant[x] for x in range(len(relevant)) for j in range(pro[x]) ]


def AgentRank(graph,iterations,d,surfNumber = 1,percentTol = 1,gamma1 = 1,gamma2 = 1) :
  """This function takes a bipartite graph as input and returns the pair of agentRanks 
  with the rank of the nodes of the two projections, computed via a random surfer approach
  in which there are two kinds of surfers, one for each graph projection. When a surfer of one
  kind encounters one or more surfer of the other kind on one of the - possible - target nodes,
  the probability of going to that node (uniform by default) is modified according to n^gamma, where n is the surfer
  population of that node. So a positive gamma means an attractive interaction, while a negative gamma means
  a repulsive intraction."""

  # instantiate some useful quantities
  projected1,projected2 = IO.projections(graph)
  projected1list=list(projected1)
  projected2list=list(projected2)
  N = max(len(projected1),len(projected2))
  prob = np.fromiter([True for i in range(int(d*100))]+[False for j in range(int((1-d)*100))],bool,100)
  pageRank1 = dict.fromkeys(projected1,(1/len(projected1))*0.1)
  pageRank2 = dict.fromkeys(projected2,(1/len(projected2))*0.1)
  it = 0 # this will serve as counter for the pagerank
  populations = dict.fromkeys(graph.nodes,0)

  # compute, once, the dict  with all the neighbors of all nodes
  neighD = {}
  for f in graph.nodes :
    neighD[f]=list(graph.neighbors(f))
  
  # compute the initial positions for the surfers of type 1
  nodes1 = rd.choice(list(projected1),size = surfNumber)
  # instantiate the list containing the surfers,
  sList1 =[surfer(x1,neighD[x1]) for x1 in nodes1]
  # compute the initial positions for the surfers of type 2
  nodes2 = rd.choice(list(projected2),size = surfNumber)
  # instantiate the list containing the surfers,
  sList2 =[surfer(x2,neighD[x2]) for x2 in nodes2]

  # refresh populations
  for n1 in nodes1 :
    populations[n1]+=1
  for n2 in nodes2 :
    populations[n2]+=1

  #main loop
  for m in range(iterations) :
    if it == 0 :
     # print("previous step: ",m)
      previous1 = copy.deepcopy(pageRank1)
      previous2 = copy.deepcopy(pageRank2)
    it += 1
    #print("it ",it)  
    

    for s1 in sList1 :
      populations[s1.n]-=1
      # if the surfer choses to do a hyperleap
      if not rd.choice(prob) or len(s1.nl) == 0 :
        s1.n = rd.choice(projected1list)
      # if the surfer choses to follow the links  
      else :
        # compute the probabilities
        pro = probabilities(s1.nl,populations,sList2,gamma1)
        #print("probs:   ",pro)
        #middle-step:
        stand1 = rd.choice(pro)
        #step
        s1.nl = neighD[stand1]  # ocio, qui dovrei togliere il precedente??
        s1.n = rd.choice(s1.nl)
      pageRank1[s1.n]+=1
      s1.nl = neighD[s1.n]
      populations[s1.n]+=1

    # loop over surfers of tipe 2
    for s2 in sList2 :
      populations[s2.n]-=1
      # if the surfer choses to do a hyperleap
      if not rd.choice(prob) or len(s2.nl) == 0 :
        s2.n = rd.choice(projected2list)
      # if the surfer choses to follow the links  
      else :
        #compute the probabilities
        pro = probabilities(s2.nl,populations,sList1,gamma2)
        #middle-step:
        stand2 = rd.choice(pro)
        #step
        s2.nl = neighD[stand2]
        s2.n = rd.choice(s2.nl)
      pageRank2[s2.n]+=1
      s2.nl = neighD[s2.n]
      populations[s2.n]+=1

    # check for convergence every 10*N iterations, where N is the number of nodes of the graph.
    if it == math.ceil(N/surfNumber) :
      it = 0  
     # print("current step",m)
      e1 = max([abs(previous1[x]-pageRank1[x])/previous1[x] for x in pageRank1])
      e2 = max([abs(previous2[y]-pageRank2[y])/previous2[y] for y in pageRank2])
     # print("error: ",e*100)
      if e1*100 < percentTol and e2*100 < percentTol :
        print("convergence reached, number of iterations: ",m+1)
        print("Final error 1: ", e1*100, " %","\nFinal error 2: ", e2*100, " %")
        break 
      
  print("iteration check ",m+1)  
  for z in pageRank1 :
    pageRank1[z] = pageRank1[z]/((m+1)*surfNumber)

  for w in pageRank2 :
    pageRank2[w] = pageRank2[w]/((m+1)*surfNumber)
  
  return pageRank1,pageRank2

