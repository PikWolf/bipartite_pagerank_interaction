## script to test the graph of anemon and fishes 
#https://www.nceas.ucsb.edu/interactionweb/html/ollerton_et_al_2007.html

import IO
import biRank as bi
import numpy as np

graph = IO.inputBiGraph('dataset/anem_fish/ollerton/matrix.txt','dataset/anem_fish/ollerton/anemon.txt','dataset/anem_fish/ollerton/fishes.txt')

dictList = []

for i in np.linspace(0,10,num=41) :
  if i == 0. :
    dictList.append(bi.AgentRank(graph,10000000,SDensity = 0.001,percentTol = 0.1,gamma1 = -10, gamma2 = -10))
  elif i > 0. :
    dictList.append(bi.AgentRank(graph,10000000,SDensity = i,percentTol = 0.1,gamma1 = -10, gamma2 = -10))

i=0

for d in dictList : 
  IO.toFile(d[0],'ollertonTest/neg_neg/'+str(i)+'_ollerton_neg_neg_SD_'+str(0.25*i)+'_tol_01_1.txt',graph)
  IO.toFile(d[1],'ollertonTest/neg_neg/'+str(i)+'_ollerton_neg_neg_SD_'+str(0.25*i)+'_tol_01_2.txt',graph)
  i+=1
