import networkx as nx
from networkx.algorithms import bipartite as bp
from scipy import sparse as sp
import numpy as np
import operator as o

###################################################################################################
def bipInputFileBiadjacency(filename) :
  """ This function takes the data of a bipartite graph from a file and returns it. """
  
  matrix = np.loadtxt(filename,delimiter='	')
  scyMatrix = sp.csr_matrix(matrix)

  return bp.from_biadjacency_matrix(scyMatrix)



def projections(graph) :
  """This function returns the two projections of the bipartite graph."""

  first_set = {n for n, d in graph.nodes(data=True) if d['bipartite']==0}
  second_set = set(graph) - first_set
  
  return (first_set,second_set)

def weightedProjections(graph) :
  """This function returns the two projections of the bipartite graph, weighted."""

  first,second = projections(graph)
  
  return  bp.weighted_projected_graph(graph,first), bp.weighted_projected_graph(graph,second)

def setAttributes(graph,attr1File,attr2File) :
  """This function sets the attributes to the two sets of nodes. attr1 and attr2 must be dictionaries."""

  first,second = projections(graph)

  subgraph1 = bp.projected_graph(graph,first)
  subgraph2 = bp.projected_graph(graph,second)

  nx.set_node_attributes(subgraph1,readAttribute(attr1File,subgraph1),"attr1")
  nx.set_node_attributes(subgraph2,readAttribute(attr2File,subgraph2),"attr2")
  return subgraph1,subgraph2

def readAttribute(filename,graph) :
  """This function returns the dictionary containing the attributes read in filename."""
  l = []
  d = {}
  with open(filename,"r") as f :
    for line in f :
      l.append(line[:-1])
  i =0
  for j in graph.nodes :
    d[j]=l[i]
    i += 1
  return d


def inputGraph(adjFile,attr1File,attr2File) :
  """This is the main input function. It takes the files containing the biadjacency matrix,the attributes of the 
  first projection and the attributes of the second projection and returns the two projected graphs."""

  bipartite_graph = bipInputFileBiadjacency(adjFile)

  return setAttributes(bipartite_graph,attr1File,attr2File)

def prov() :
  """Funzione provvisioria che metto per comodità. Specifica per il grafo nazioni/prodotti."""

  g = inputBiGraph('dataset/np/matrix.txt','dataset/np/countries.txt','dataset/np/products.txt')
  g.remove_nodes_from([3,128,99,120,53,90,5])
  return g
##############################################################################################################

def setBiGraphAttributes(biGraph,attr1File,attr2File) :
  """This function takes as an argument a bipartite netorkx graph and the filenames of the files with the attributes, 
  and returns the graph with the correct attributes."""

  first,second = projections(biGraph)
  subgraph1 = bp.projected_graph(biGraph,first)
  subgraph2 = bp.projected_graph(biGraph,second)
  
  attr1 = readAttribute(attr1File,subgraph1)
  attr2 = readAttribute(attr2File,subgraph2)

  attrDict = dict(list(attr1.items())+list(attr2.items()))

  nx.set_node_attributes(biGraph,attrDict,"id")
  return biGraph 

def inputBiGraph(matrixFile,attr1File,attr2File) : 
  bipGraph = bipInputFileBiadjacency(matrixFile)

  return setBiGraphAttributes(bipGraph,attr1File,attr2File)

###############################################################################################################
### Now some functions to manage the output of the pagerank and agentrank and do the comparison ###############

def rank(rankDict,nodes = False) :
  """This function takes the dictionary that is the output of the pagerank or agentrank and sort it according to
  the rank of the nodes, i.e, according to the values of the dictionary.If nodes = True only an ordered list of the 
  nodes is returned, otherwise a list of tubles ordered by the second element, i.e. the rank."""
  
  dictS = sorted(rankDict.items(), key = o.itemgetter(1),reverse=True)
  
  if nodes :
    return [i[0] for i in dictS]
  else :
    return dictS

def toFile(rankDict,filename,graph,nodes = False) :
  """This function print the results to the file named filename."""

  with open(filename,'w') as f :
    for i in rank(rankDict,nodes) :
      f.write(str(i[0])+'\t'+str(i[1])+'\t'+graph.nodes[i[0]]['id'])
      f.write('\n')
  return

def rankAttirbutes(rankDict,graph,attributeID) :
  """This function takes the dictionary taht is the output of the agentRank and sort it according to the rank of the
  nodes, i. e. according to the values of the dictionary. It also need the graph in order to link the nodes to the 
  attributes. It then returns the ordered list of the attributes required."""

  sort = rank(rankDict,nodes = True)
  return [graph.nodes[x][attributeID] for x in sort]

def sortMatrix(array2D,rowRank,columnRank) :
  """This function rearrange the column and rows of the matrix, according to the respective ranks. """
  
  rankIndexesR = np.argsort(rowRank)
  rankIndexesC = np.argsort(columnRank)
  matrix = array2D[:,rankIndexesC]
  return matrix[rankIndexesR,:]

def rankToMatrix(rankRow,rankColumn,graph) :

 matrix = bp.biadjacency_matrix(graph,rankRow,rankColumn)
 return matrix.toarray()

def filesToMatrix(rankFile1,rankFile2,graph) :

  rank1 = []
  rank2 = []
  with open(rankFile1) as f1 :
    for line in f1 :
      rank1.append(int(line.split(None,1)[0]))
  with open(rankFile2) as f2 :
    for line in f2 :
      rank2.append(int(line.split(None,1)[0]))
  print(rank1)
  print('\n\n\n')
  print(rank2)
  matrix = rankToMatrix(rank1,rank2,graph)
  return matrix

def fileToRank(rankFile1) :

  rank1 = []
  with open(rankFile1) as f1 :
    for line in f1 :
      rank1.append(int(line.split(None,1)[0]))

  return rank1

def fileToRankTot(rankFile1) :

  rank1 = {}
  with open(rankFile1) as f1 :
    for line in f1 :
      rank1[int(line.split(None,1)[0])] = float(line.split(None,2)[1])

  return rank1

def spearmanDictToFile(sDict,filename) :  # xArray are the values of the parameter (es. gamma or SDensity)
  with open(filename,'w') as f :
    i = 0
    for s in sDict :
      parameter = s.split(sep = '_',maxsplit = 6)[4]
      f.write(s+'\t\t'+parameter+'\t\t'+str(sDict[s])+'\n')
      i+=1
