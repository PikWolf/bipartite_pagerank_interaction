# some useful functions to analyse the data

import scipy.stats as st
import IO
import biRank as bi
import os
import networkx as nx
from networkx import bipartite as bp

def spearman(rank1,rank2) :
  return st.spearmanr(rank1,rank2)

def spearFiles(file1,file2) :
  """Function that computes the spearman correlation taking the input from
  the files containing the two ranks."""

  rank1 = IO.fileToRank(file1)
  rank2 = IO.fileToRank(file2)
  return spearman(rank1,rank2)

def spearDegree(filename,graph,i) :
  """Function that computes the spearman correlation between the inout and 
  the degree ordering."""

  rank = IO.fileToRank(filename)
  degRank = bi.degreeRank(graph)[i]

  return spearman(rank,degRank)

def spearPageRank(rank,graph,i) :
  pagerank = IO.rank(nx.pagerank(bp.projected_graph(graph,IO.projections(graph)[i])),nodes=True)
  print(len(rank),len(pagerank))
  return spearman(rank,pagerank)

def spearManyFiles(dirPath,graph) :
  """Given the path of a directory, this function read all the .txt files, assuming that they are 
  data files, and returns a dictionary with filenames as keys and tuple of speraman correlation and
  p-value as values."""
  
  spearDict = {}
  degrank = bi.degreeRank(graph)
  for entry1 in os.scandir(dirPath) :
    if entry1.is_dir()  and not (entry1.name == 'oth'):
      for entry in os.scandir(dirPath+'/'+entry1.name) :
        if entry.name[-3:] == 'txt' :
          rank = IO.fileToRank(entry)
          if int(entry.name[-5]) == 1:
            spearDict[entry1.name+'/'+entry.name] = tuple(spearman(degrank[0],rank))
          else :
            spearDict[entry1.name+'/'+entry.name] = tuple(spearman(degrank[1],rank))
  return spearDict

def spearManyFilesRef(dirPath,graph,refRankFile) :
  """Given the path of a directory, this function read all the .txt files, assuming that they are 
  data files, and returns a dictionary with filenames as keys and tuple of speraman correlation and
  p-value as values."""
  
  spearDict = {}
  refRank = IO.fileToRank(refRankFile)
  for entry1 in os.scandir(dirPath) :
    if entry1.is_dir()  and not (entry1.name == 'oth'):
      for entry in os.scandir(dirPath+'/'+entry1.name) :
        if entry.name[-3:] == 'txt' :
          rank = IO.fileToRank(entry)
          if int(entry.name[-5]) == 1:
            spearDict[entry1.name+'/'+entry.name] = tuple(spearman(refRank,rank))
  return spearDict

def spearManyFilesRef1(dirPath,graph,refRankFile) :
  """Given the path of a directory, this function read all the .txt files, assuming that they are 
  data files, and returns a dictionary with filenames as keys and tuple of speraman correlation and
  p-value as values."""
  
  spearDict = {}
  refRank = IO.fileToRank(refRankFile)
  for entry in os.scandir(dirPath) :
        if entry.is_file() :
          if entry.name[-3:] == 'txt' :
            rank = IO.fileToRank(dirPath+"/"+entry.name)
            if int(entry.name[-5]) == 1:
              spearDict[entry.name] = tuple(spearman(refRank,rank))
  return spearDict

def averageRank(dirPath,nodes1,nodes2) :
  """Given the path of a directory, this function read all the .txt files and return a dictinary
  with the average rank for each node."""

  totDict1 = dict.fromkeys(nodes1,0.0)
  totDict2 = dict.fromkeys(nodes2,0.0)
  n1,n2 = 0,0
  for entry in os.scandir(dirPath) :
    if entry.is_file()  and entry.name[-3:] == 'txt' :
        if entry.name[-5] == '1' :
          n1+=1
          with open(dirPath+'/'+entry.name,'r') as f :
            for line in f :
              stra = line.split(None,2)
              print(stra)
              totDict1[int(stra[0])]+=float(stra[1])
        elif entry.name[-5] == '2' :
          n2+=1
          with open(dirPath+'/'+entry.name,'r') as f :
            for line in f :
              stra = line.split(None,2)
              totDict2[int(stra[0])]+=float(stra[1])
     
  dict1 = {x: totDict1[x]/n1 for x in totDict1}
  dict2 = {x: totDict2[x]/n2 for x in totDict2}
  return dict1,dict2

def mySpearman(rank1,rank2) :
   """My own implementation of spearman correlation."""
  
   variables = set(rank1)
 #  print('variables',variables)
   N = len(variables)
   index1 = [rank1.index(i)+1 for i in variables]
   index2 = [rank2.index(i)+1 for i in variables]
#   print('index1',index1,'index2',index2)
   D = sum([(index1[j]-index2[j])**2 for j in range(N)])
   spearmanC = 1-6*D/(N*(N*N-1))
   return spearmanC

def mySpearManyFilesRef(dirPath,graph,refRankFile,proN=1) :
  """Given the path of a directory, this function read all the .txt files, assuming that they are 
  data files, and returns a dictionary with filenames as keys and tuple of speraman correlation and
  p-value as values. here using my own implementation."""
  
  spearDict = {}
  refRank = IO.fileToRank(refRankFile)
  for entry in os.scandir(dirPath) :
        if entry.is_file() :
          if entry.name[-3:] == 'txt' and not entry.name[-5] == 'g' :
            if int(entry.name[-5]) == proN:
              print(entry.name)
              rank = IO.fileToRank(dirPath+"/"+entry.name)
              spearDict[entry.name] = mySpearman(refRank,rank)
              # print(rank)
  return spearDict

def nestedness(mat) :
  nestedness = 0
  y = mat.shape[0]
  for i in range(mat.shape[1]) :
    for j in range(y-1,-1,-1) :
      if mat[j][i] == 0 :
        nestedness += 1
      else :
        break
  return nestedness

def nestednessFiles(rankFile1,rankFile2,graph) :
  
  mat = IO.filesToMatrix(rankFile1,rankFile2,graph)
  return nestedness(mat)
      
  
