import sys
import biRank as bi
from biRank import IO 

sN1 = int(sys.argv[1])
print("surfNumber1:",sN1)
sN2 = int(sys.argv[2])
print("surfNumber2:",sN2)
tol = float(sys.argv[3])
print("tolerance:",tol)
g1 = int(sys.argv[4])
print("gamma1: ",g1)
g2 = int(sys.argv[5])
print("gamma2: ",g2)
outputFile1 = str(sys.argv[6])
print('file1:', outputFile1)
outputFile2 = str(sys.argv[7])
print('file2:', outputFile2)

graph = IO.prov()

rank1,rank2 = bi.AgentRank(graph,1000000,surfNumber1 = sN1,surfNumber2 = sN2percentTol = tol,gamma1 = g1,gamma2 = g2)

IO.toFile(rank1,outputFile1,graph)
IO.toFile(rank2,outputFile2,graph)

