# some useful function to display data and results.

import matplotlib.pyplot as plt
import networkx as nx
import numpy as np

def matrixPlot(array2D) :
  """ Function to plot a (better if binary) matrix."""

  fig = plt.figure()
  ax = fig.add_subplot(111)
  plt.gca().invert_yaxis()
  ax.pcolormesh(array2D,cmap = 'Greys')

